﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DayOneInfo.Data.Model
{
    public class EmailConfig
    {
        public string To { get; set; }

        public string CC { get; set; }

        public string ApiSendMail { get; set; }

        public string FolderAttachEmail { get; set; }

        public string ContentType { get; set; }
    }
}
