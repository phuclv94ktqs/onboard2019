﻿using Framework.Common.ApiResult;
using Framework.Common.JWT;
using Framework.Common.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DayOneInfo.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IJwtTokenManager _jwtTokenManager;
        private readonly ILogger _logger;
        public LoginController(IJwtTokenManager jwtTokenManager,
                ILogger<LoginController> logger)
        {
            _jwtTokenManager = jwtTokenManager;
            _logger = logger;
        }
        [AllowAnonymous]
        [HttpPost("auth")]
        public async Task<ApiResult> Authenticate([FromBody]LoginModel model)
        {
            // Gen token
            var clams = new List<Claim>
            {
                new Claim(ClaimTypes.Name,model.Username)
            };
            if (model.Username.IndexOf("@fsoft.com.vn") == -1)
            {
                return new ApiResult
                {
                    Data = null,
                    Message = Message.NoAuthorize,
                    Status = HttpStatus.NoAuthorize
                };
            }
            var token = _jwtTokenManager.GenerateToken(clams);

            return new ApiResult
            {
                Data = new
                {
                    token = token,
                    AppCode = "VN",
                    User = model.Username
                },
                Status = HttpStatus.OK
            };
        }
    }
}