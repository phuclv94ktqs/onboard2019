﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.Globalization;

namespace Framework.Common
{
    public class Utility
    {
        public static string DateFormat(DateTime dt)
        {
            if (dt >= Convert.ToDateTime("01-01-1920"))
                return dt.ToString("yyyy-MM-dd");
            else
                return string.Empty;
        }

        public static string DateFormat(string dt)
        {
            if (!string.IsNullOrEmpty(dt))
            {
                return Convert.ToDateTime(dt).ToString("yyyy-MM-dd");
            }
            return string.Empty;
        }

        public static string DateTimeFormat(DateTime dt)
        {
            if (dt >= Convert.ToDateTime("01-01-1920"))
                return dt.ToString("dd MMM, yyyy HH:mm");
            else
                return string.Empty;
        }

        public static string DateTimeFormat(string dt)
        {
            if (string.IsNullOrEmpty(dt))
            {
                return string.Empty;
            }
            return Convert.ToDateTime(dt).ToString("dd MMM, yyyy HH:mm");
        }

        public static string GetJson(DataTable dt)
        {
            return JsonConvert.SerializeObject(dt, Formatting.Indented);
        }

        public static DataTable GetDataTableFromJson(string json)
        {
            return (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));
        }

        public static DateTime StringtoDate(String str)
        {
            if (String.IsNullOrEmpty(str))
            {
                return new DateTime();
            }

            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            dtfi.ShortDatePattern = "MM/dd/yyyy";
            dtfi.DateSeparator = "/";
            return Convert.ToDateTime(str, dtfi);
        }
    }
}
