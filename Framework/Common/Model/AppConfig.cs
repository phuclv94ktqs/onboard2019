﻿using DayOneInfo.Data.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.Common.Model
{
    public class AppConfig
    {
        public string ApplicationCode { get; set; }

        public bool IsTestEnvironment { get; set; }

        public string AppUrl { get; set; }

        public string PermissionServiceUrl { get; set; }

        public EmailConfig EmailConfig { get; set; }

        public bool CheckORE { get; set; }

        public string LinkForNewORE { get; set; }

        public string LinkForRenewORE { get; set; }

        public bool AutoApprove { get; set; }
    }
}
