import { Button, Checkbox, Form, Icon, Input } from "antd";
import React, { Component } from "react";
import logoLogin from '../../Assets/Images/FPT.png';
import { AuthContext } from "../../context";
import Authencation from '../../Services/Authencation';
import Notification from '../../Services/Notification';
import './Login.css';

export class Login extends Component {
    static contextType = AuthContext;

    constructor(props) {
        super(props);
        this.AuthenApi = new Authencation();
        this.Notification = new Notification();
    }

    state = {
        loading: false
    }
    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, value) => {
            if (!err) {
                this.setState({ loading: true });
                this.AuthenApi.login(value).then(res => {
                    this.setState({ loading: false });
                    if (res.status === 200 && res.data && res.data.token) {
                        localStorage.setItem('current-user', JSON.stringify(res.data));
                        this.context.onLogin();
                    }
                    else {
                        this.Notification.error("Wrong username or password")
                    }
                });
            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div className="page-content">
                <div className="gray-bg">
                    <div className="middle-box text-center loginscreen animated fadeInDown">
                        <div>
                            <div className="logo-name">
                                <img src={logoLogin} alt={logoLogin} />
                            </div>
                        </div>
                        <h3>FPT Software Company Limited[VN]</h3>
                        <div style={{ marginBottom: 10 }}> Sign in with your organizational account </div>
                        <Form onSubmit={this.handleSubmit} className="login-form" >
                            <Form.Item className="form-item" > {
                                getFieldDecorator("username", {
                                    rules: [
                                        { required: true, message: "Please input your email!" }
                                    ]
                                })(<
                                    Input prefix={<
                                        Icon
                                        type="mail"
                                        style={
                                            { color: "rgba(0,0,0,.25)" }}
                                    />
                                    }
                                    placeholder="Email" /
                                >
                                )
                            }
                            </Form.Item>
                            <Form.Item className="form-item" > {
                                getFieldDecorator("password", {
                                    rules: [
                                        { required: true, message: "Please input your Password!" }
                                    ]
                                })(<
                                    Input prefix={<
                                        Icon
                                        type="lock"
                                        style={
                                            { color: "rgba(0,0,0,.25)" }}
                                    />
                                    }
                                    type="password"
                                    placeholder="Password" /
                                >
                                )
                            }
                            </Form.Item>
                            <Form.Item className="form-item" >
                                <div > {
                                    getFieldDecorator("remember", {
                                        valuePropName: "checked",
                                        initialValue: true
                                    })(<Checkbox> Remember me </Checkbox>)}
                                </div>

                                <Button type="primary"
                                    htmlType="submit"
                                    className="login-form-button"
                                    loading={this.state.loading} >
                                    Log in
                                    </Button>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </div>
        );
    }
}
export default Form.create({ name: "normal_login" })(Login);