function HomePage(props) {
    const Component = require('./' + constant.GetAppCode() + '/Home').default;
    return (
        <Component useSuspense={false} {...props} />
    )
}
export default HomePage