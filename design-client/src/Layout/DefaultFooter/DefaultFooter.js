import { Button, Col, Icon, Row } from 'antd';
import React from 'react';

function DefaultFooter(props) {
  return (
    <>
      <hr />
      <Row style={{ 'color': 'green' }} gutter={2}>
        <Col span={11} style={{ 'color': 'black' }} xs={24} sm={24} md={8} lg={11}>
          <strong>Copyright </strong> FPT Software &copy; 2019
        </Col>
        <Col span={2} xs={24} sm={24} md={3} lg={2}>
          < Button type="link" > <Icon type="question-circle" /> {'Trợ giúp'}</Button>                        

        </Col>
        <Col span={4} xs={24} sm={24} md={4} lg={4}>
          < Button type="link" > <Icon type="mail" /> {'Gửi yêu cầu hỗ trợ'}</Button>
        </Col>
        <Col span={3} xs={24} sm={24} md={4} lg={3}>
          < Button type="link" > <Icon type="form" /> {'Tạo phản hồi'}</Button>

        </Col>
        <Col span={4} xs={24} sm={24} md={5} lg={4}>
          < Button type="link" > <Icon type="wechat" /> {'Liên hệ hỗ trợ'}</Button>

        </Col>
      </Row>
    </>
  )
}

export default DefaultFooter

