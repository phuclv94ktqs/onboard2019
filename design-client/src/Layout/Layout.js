import React from "react";
import { Route, Switch } from "react-router-dom";
import routes from "../routes";
import DefaultLayout from "./DefaultLayout";

function Layout() {
    return (
        <DefaultLayout>
            <Switch>
                {routes.map((route, index) => (
                    <Route
                        component={route.render}
                        {...route}
                        key={"routes-" + index}
                    />
                ))}
            </Switch>
        </DefaultLayout>
    );
}

export default Layout;