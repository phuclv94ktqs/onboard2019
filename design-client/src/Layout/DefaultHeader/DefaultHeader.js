import { Button, Col, Dropdown, Icon, Menu, Row } from 'antd';
import React, { PureComponent } from 'react';
import { AuthContext } from '../../context';

export class DefaultHeader extends PureComponent {
  static contextType = AuthContext;
  onLogout = () => {
    this.context.onLogout()
  }
  render() {
    const t = this.props.t
    const languages = (
      <Menu>
        <Menu.Item>
          <Button type="link">Tiếng Việt</Button>
        </Menu.Item>
      </Menu>
    )
    return (
      <>
        <Row>
          <Col span={2} xs={1}>
            <Icon
              className="trigger"
              type='menu-fold'
              onClick={this.props.toggleCollapsed}
            />
          </Col>
          <Col style={{ float: 'right' }}>
            <Button type="link" style={{ padding: '10px' }} title={'Help'}><Icon type="question-circle" style={{ fontSize: '15px' }} /></Button>
            {/*     
                <Dropdown overlay={languages}>
                  <Button type="link"  style={{ padding: '10px' }}><Icon type="global" style={{ fontSize: '15px' }} /></Button>
                </Dropdown> */}
            <Button type="link" onClick={this.onLogout} style={{ padding: '10px' }}> <Icon type="logout" style={{ fontSize: '15px' }} /> {'Đăng xuất'}</Button>

          </Col>
        </Row>

      </>
    )
  }
}
export default DefaultHeader
