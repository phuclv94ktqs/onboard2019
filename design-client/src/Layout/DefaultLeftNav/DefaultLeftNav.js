import { Button, Icon, Menu } from 'antd';
import React, { PureComponent } from 'react';
import { Link, withRouter } from "react-router-dom";
import logo from "../../Assets/Images/FPT.png";
import { AuthContext } from "../../context";
import Account from "./Account";


export class DefaultLeftNav extends PureComponent {
    static contextType = AuthContext;
    state = {
        selectedKeys: "/"
    };
    componentDidMount() {
        if (this.state.selectedKeys !== this.props.location.pathname) {
            this.setState({ selectedKeys: this.props.location.pathname });
        }
    }
    componentDidUpdate() {
        if (this.state.selectedKeys !== this.props.location.pathname) {
            this.setState({ selectedKeys: this.props.location.pathname });
        }
    }
    onClickMenu = ({ item, key, keyPath }) => {
        if (this.props.history) {
            this.props.history.push(key);
        }
    };

    onLogout = () => {
        this.context.onLogout()
    }
    render() {
        const t = this.props.t
        const menu = (
            <Menu>
                <Menu.Item>
                    <Button type="link" onClick={this.onLogout}>{'Đăng xuất'}</Button>
                </Menu.Item>
            </Menu>
        )
        let logoItem = (
            <div className='logo-element'>
                <Link to='/'>
                    <img src={logo} alt="logo" style={{ width: '40px' }} />
                </Link>
            </div>
        )
        if (!this.props.collapsed) {
            logoItem = (
                <Account menu={menu} />
            )
        }
        return (
            <>
                <div>
                    {logoItem}
                </div>
                <Menu
                    selectedKeys={[this.state.selectedKeys]}
                    onClick={this.onClickMenu}
                    mode="inline"
                    theme="dark"
                >
                    <Menu.Item key="/">
                        <Icon type="home" />
                        <span className="nav-text">{'Trang Chủ'}</span>
                    </Menu.Item>
                </Menu>
            </>
        )
    }
}
export default withRouter(DefaultLeftNav);
