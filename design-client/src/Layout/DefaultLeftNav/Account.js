import React, { useContext } from 'react'
import avatar from "../../Assets/Images/avatar.png";
import { Icon, Dropdown, Button } from "antd";
import { AppContext } from '../../context';


export default function Account(props) {
    const value = useContext(AppContext);

    return (
        <div className='dropdown profile-element'>
            <div>
                <Dropdown overlay={props.menu}>
                    <Button type="link">{value.userInfo ? value.userInfo.account : "PhucLV2"} <Icon type="down" /></Button>
                </Dropdown>
            </div>
        </div>
    )
}
