import { Layout, LocaleProvider } from "antd";
import en_US from 'antd/lib/locale-provider/en_US';
import vi_VN from 'antd/lib/locale-provider/vi_VN';
import React, { useEffect, useState } from "react";
import { AppContext } from "../context";
import { Authencation } from "../Services/Authencation";
import DefaultFooter from "./DefaultFooter/DefaultFooter";
import DefaultHeader from "./DefaultHeader/DefaultHeader";
import DefaultLeftNav from "./DefaultLeftNav/DefaultLeftNav";

const { Header, Content, Footer, Sider } = Layout;

function DefaultLayout(props) {
  const [collapsed, setCollapsed] = useState(true);
  const [collapsedWidth, setCollapsedWidth] = useState(80);
  const [user, setUser] = useState();
  const [language, setLanguage] = useState(en_US);

  function toggleCollapsed() {
    setCollapsed(!collapsed);
  }

  function updateDimensions() {
    console.log(window.innerWidth < 800)
    if (window.innerWidth < 800) {
      setCollapsed(true)
    }
    setCollapsedWidth(window.innerWidth < 600 ? 0 : 80)
  }

  useEffect(() => {
    window.addEventListener("resize", updateDimensions);
    return () => {
      window.removeEventListener("resize", updateDimensions);
    };
  }, [])

  useEffect(() => {
    setUser(getUserInfo());
    setLanguage(getLanguage())
  }, [])

  //#region language
  function getLanguage() {
    var lng = localStorage.getItem('language')
    return getLanguageFromString(lng)
  }

  function changeLanguage(lang) {
    setLanguage(getLanguageFromString(lang));
    localStorage.setItem('language', lang);
  }

  function getLanguageFromString(lang) {
    if (lang) {
      if (lang === 'vi_VN') return vi_VN;
      return en_US;
    }
    else {
      localStorage.setItem('language', 'en_US');
      return en_US;
    }
  }
  //#endregion language

  function getUserInfo() {
    var currentUser = null;
    if (localStorage.getItem('current-user')) {
      currentUser = JSON.parse(localStorage.getItem('current-user'));
    }
    new Authencation().getUserInfo(currentUser.user)
      .then(res => {
        if (res && res.data && res.status === 200) {
          setUser(res.data);
        }
      });
  } 

  return (
    <AppContext.Provider value={{ userInfo: user, language: language }}>
      <LocaleProvider locale={language}>
        <Layout>
          <Sider
            trigger={null}
            collapsible
            collapsed={collapsed}
            className='left-nav'
            collapsedWidth={collapsedWidth}
          >
            <DefaultLeftNav useSuspense={false} collapsed={collapsed} />
          </Sider>
          <Layout>
            <Header style={{ background: "#fff", padding: 0 }}>
              <DefaultHeader changeLanguage={changeLanguage} useSuspense={false} toggleCollapsed={toggleCollapsed} />
            </Header>
            <Content
              style={{
                margin: "20px  24px 10px",
                minHeight: 280
              }}
            >
              {props.children}

            </Content>
            <Footer>
              <DefaultFooter useSuspense={false} />
            </Footer>
          </Layout>
        </Layout>
      </LocaleProvider>
    </AppContext.Provider>
  );
}

export default DefaultLayout;
