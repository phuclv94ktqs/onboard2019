import React, { useState } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import './App.css';
import { AuthContext } from './context';
import Layout from './Layout/Layout';
import Login from './Modules/Login/Login';
import './Assets/app.css';

function App() {
  let token = getToken();
  const [isAuth, setAuth] = useState(token !== null);
  function onLogin() {
    setAuth(true);
  }
  function onLogout() {
    localStorage.removeItem("current-user");
    setAuth(false);
  }
  function getToken() {
    let access_token = null;
    const currentUser = localStorage.getItem('current-user');
    if (currentUser) {
      let user = JSON.parse(currentUser);
      if (user.token) {
        access_token = user.token;
      }
    }
    return access_token;
  }
  return (
    <Router>
      <AuthContext.Provider value={{ isAuth, onLogin, onLogout }}>
        {isAuth
          ? <Layout />
          : <Login />
        }
      </AuthContext.Provider>
    </Router>
  );
}

export default App;
