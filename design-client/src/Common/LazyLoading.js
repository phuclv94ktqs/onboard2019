import React from "react";
import { Spin } from "antd";

const MySpinner = <Spin />;
const LazyLoading = importFunc => {
    const Component = React.lazy(importFunc);
    return props => (
        <React.Suspense fallback={MySpinner}>
            <Component {...props} />
        </React.Suspense>
    );
};
export default LazyLoading;