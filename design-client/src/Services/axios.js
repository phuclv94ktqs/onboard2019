import axios from 'axios';

const instance = axios.create({
    baseURL: process.env.REACT_APP_API_URL
});

instance.interceptors.request.use(request => {
    let currentUser = null;
    if (localStorage.getItem('current-user')) {
        currentUser = JSON.parse(localStorage.getItem('current-user'));
    }
    if (currentUser && currentUser.token) {
        request.headers['Authorization'] = `Bearer ${currentUser.token}`;
    }
    return request;
}, error => {
    console.error(error);
    return Promise.reject(error);
});
instance.interceptors.response.use(response => {
    return response.data;
}, error => {
    if(error.response && error.response.status === 401){
        localStorage.removeItem("current-user");
        window.location.reload();
    }
    return Promise.reject(error);
});
export default instance;