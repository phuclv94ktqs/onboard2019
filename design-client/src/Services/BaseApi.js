import { Component } from 'react';
import axios from './axios';

export class BaseApi extends Component {
    execute_get = async url => {
        return await axios.get(url);
    };

    execute_post = async (url, model) => {

        return await axios.post(url, model);
    };

    execute_put = async (url, model) => {
        return await axios.put(url, model);
    };

    execute_delete = async (url, model) => {
        return await axios.delete(url, model);
    };

    uploadFile = async (url, file) => {
        var data = new FormData();
        data.append("file", file);
        return await axios.post(url, data);
    };

}