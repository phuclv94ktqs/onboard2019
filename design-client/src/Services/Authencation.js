import axios from 'axios';

const baseURL = process.env.REACT_APP_API_URL;
const baseAutURL = process.env.REACT_APP_AUTH_URL;

const  instance = axios.create();
instance.interceptors.response.use(response => {
    return response.data;
},error => {
    if(error.response && error.response.status === 401){
        localStorage.removeItem("current-user");
        window.location.reload();
    }
    return Promise.reject(error)
})

instance.interceptors.request.use(request => {
    let currentUser = null
    if(localStorage.getItem('tss-current-user')){
      currentUser = JSON.parse(localStorage.getItem('current-user'));
    }
    if (currentUser && currentUser.token) {
        request.headers['Authorization'] = `Bearer ${ currentUser.token }`;

    } 
     return request;
    },error => {
    console.error(error);
    return Promise.reject(error)
})


export class Authencation {
    login = async (model) => {
        return instance.post(baseAutURL , model)
    }
    getUserInfo = async model => {
        return instance.get(baseURL + '/api/UserSyncAd/getUserInfo?account=' + model)
    }
}

export default Authencation
