export function GetAppCode() {
    var currentUser = null;
    if (localStorage.getItem('current-user')) {
        currentUser = JSON.parse(localStorage.getItem('current-user'));
    }
    if(currentUser && currentUser.appCode){
        return currentUser.appCode;
    }
    return "VN";
}